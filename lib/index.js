const addon = require("../native");

class Db {
  #db;

  constructor() {
    this.#db = addon.db_new();
  }

  insert(key, value) {
    addon.db_insert(this.#db, key, value);
  }

  get(key) {
    return addon.db_get(this.#db, key);
  }
}

module.exports = Db;
