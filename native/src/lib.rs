use neon::prelude::*;
use std::cell::RefCell;
use std::collections::BTreeMap;

struct Db(BTreeMap<String, String>);
impl Finalize for Db {}

type BoxedDb = JsBox<RefCell<Db>>;

#[allow(clippy::unnecessary_wraps)]
fn db_new(mut cx: FunctionContext) -> JsResult<BoxedDb> {
    let db = Db(BTreeMap::new());
    Ok(cx.boxed(RefCell::new(db)))
}

fn db_insert(mut cx: FunctionContext) -> JsResult<JsUndefined> {
    let handle = cx.argument::<BoxedDb>(0)?;
    let mut db = handle.borrow_mut();

    let key = cx.argument::<JsString>(1)?.value(&mut cx);
    let value = cx.argument::<JsString>(2)?.value(&mut cx);
    db.0.insert(key, value);

    Ok(cx.undefined())
}

fn db_get(mut cx: FunctionContext) -> JsResult<JsValue> {
    let handle = cx.argument::<BoxedDb>(0)?;
    let db = handle.borrow();

    let key = cx.argument::<JsString>(1)?.value(&mut cx);
    let value = db.0.get(&key);

    match value {
        Some(value) => Ok(cx.string(value).upcast()),
        None => Ok(cx.null().upcast()),
    }
}

register_module!(mut cx, {
    cx.export_function("db_new", db_new)?;
    cx.export_function("db_insert", db_insert)?;
    cx.export_function("db_get", db_get)?;

    Ok(())
});
